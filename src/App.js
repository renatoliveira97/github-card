import './App.css';
import { useState } from 'react';

import * as yup from 'yup';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';

import axios from 'axios';

const App = () => {

  const [repositories, setRepositories] = useState([]);
  const [errorMsg, setErrorMsg] = useState('');

  const formSchema = yup.object().shape({
    name: yup.string(),
  });

  const {
    register,
    handleSubmit
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const handleForm = (data) => {
    if (data.name.length === 0) {
      setErrorMsg('Nome do repositório obrigatório');
    } else {
      axios
      .get(`https://api.github.com/repos/${data.name}`)
      .then((response) => {
        setErrorMsg('');
        setRepositories([...repositories, response.data]);
        console.log(repositories);
      })
      .catch((error) => {
        setErrorMsg('Erro ao buscar pelo repositório');
      });
    }    
  }

  return (
    <div className="App">
      <header className="App-header">
        <form onSubmit={handleSubmit(handleForm)}>
          <input type="text" className="text-box" {...register('name')}/>
          <button type="submit" className="btn-submit">Pesquisar</button>
        </form>
        <p className="error-msg">{errorMsg}</p>
        <div>
        {repositories.map((repo) => {
            return (
              <div className="card-container" key={repo.id}>
                <img className="card-img" src={repo.owner.avatar_url} alt={repo.owner.login}/>
                <div className="description-container">
                  <p className="card-name">{repo.full_name}</p>
                  <p className="card-description">{repo.description}</p>
                </div>
              </div>
            );
          })}
        </div>
      </header>
    </div>
  );
}

export default App;
